package br.com.caelum.jdbc.teste;

import java.text.SimpleDateFormat;

import br.com.caelum.jdbc.dao.ContatoDao;
import br.com.caelum.jdbc.modelo.Contato;

public class TestaPesquisar {

	public static void main(String[] args) {

		ContatoDao dao = new ContatoDao();
		Contato contato = dao.pesquisa(2);

		if (contato != null) {
			System.out.println("Nome: " + contato.getNome());
			System.out.println("Email: " + contato.getEmail());
			System.out.println("Endereço: " + contato.getEndereco());
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String data = sdf.format(contato.getDataNascimento().getTimeInMillis());
			System.out.println("Data de Nascimento: " + data + "\n");
		} else {
			System.out.println("Contato não registrado");
		}
	}
}
