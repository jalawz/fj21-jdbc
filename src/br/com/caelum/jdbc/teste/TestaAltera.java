package br.com.caelum.jdbc.teste;

import java.util.Calendar;

import br.com.caelum.jdbc.dao.ContatoDao;
import br.com.caelum.jdbc.modelo.Contato;

public class TestaAltera {
	
	public static void main(String[] args) {
		
		ContatoDao dao = new ContatoDao();
		Contato contato = new Contato();
		contato.setId(3L);
		contato.setNome("Docket");
		contato.setEmail("contato@docket.com.br");
		contato.setEndereco("Pinheiros");
		Calendar data = Calendar.getInstance();
		contato.setDataNascimento(data);
		
		dao.altera(contato);
		
		System.out.println("Contato: " + contato.getNome() + " alterado!");
	}
}
