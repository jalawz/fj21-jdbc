package br.com.caelum.jdbc.teste;

import java.util.Calendar;

import br.com.caelum.jdbc.dao.ContatoDao;
import br.com.caelum.jdbc.modelo.Contato;

public class TestaInsere {
	
	public static void main(String[] args) {
		
		// pronto para gravar
		Contato contato = new Contato();
		contato.setNome("HDI Seguros");
		contato.setEmail("contato@hdi.com.br");
		contato.setEndereco("Av. das Nações Unidas 1500 cj22");
		contato.setDataNascimento(Calendar.getInstance());
		
		// grave nessa conexao
		ContatoDao dao = new ContatoDao();
		
		// método elegante
		dao.adiciona(contato);
		
		System.out.println("Gravado!");
	}
}
