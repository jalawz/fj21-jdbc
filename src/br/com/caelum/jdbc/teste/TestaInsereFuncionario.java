package br.com.caelum.jdbc.teste;

import br.com.caelum.jdbc.dao.FuncionarioDao;
import br.com.caelum.jdbc.modelo.Funcionario;

public class TestaInsereFuncionario {
	
	public static void main(String[] args) {
		Funcionario funcionario = new Funcionario();
		funcionario.setNome("Paulo Roberto Menezes");
		funcionario.setUsuario("paulo.roberto");
		funcionario.setSenha("senhazita");
		
		FuncionarioDao dao = new FuncionarioDao();
		dao.adiciona(funcionario);
		
		System.out.println("Adicionado!");
	}
}
